/**  
 * All rights Reserved
 * @Title:  RedisJava.java   
 * @Package test   
 * @Description:    TODO  
 * @author: liuyh     
 * @date:   2019年5月30日 下午3:23:37   
 * @version V1.0 
 * @Copyright: 2019. All rights reserved. 
 * 注意：本内容仅限于北京联通时科信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;









import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPipeline;
import redis.clients.jedis.Transaction;

/**   
 * @ClassName:  RedisJava   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: liuyh
 * @date:   2019年5月30日 下午3:23:37   
 *     
 * @Copyright: 2019 . All rights reserved. 
 * 注意：本内容仅限于北京联通时科信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目 
 */
public class RedisJava {
	public static void main(String[] args) {
		Jedis jedis = new Jedis("localhost");
		System.out.println("链接成功");
		System.out.println("服务正在运行："+jedis.ping());
		//key
		jedis.set("runoobkey", "www.runoob.com");
		System.out.println("redis 存储的字符串为："+jedis.get("runoobkey"));
		//list
		jedis.lpush("site-list", "Runoob");
		jedis.lpush("site-list", "Google");
		jedis.lpush("site-list", "Taobao");
		List<String> list=jedis.lrange("site-list", 0, 2);
		for(int i=0; i<list.size(); i++){
			System.out.println("列表项为："+list.get(i));
		}
		//keys
		Set<String> keys = jedis.keys("*");
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
			String key =  it.next();
			System.out.println(key);
		}
		
		//基本同步方式
		long start = System.currentTimeMillis();
		for(int i=0;i<10000;i++){
			String result = jedis.set("n"+i, "n"+i);
			System.out.println(result);
		}
		long end = System.currentTimeMillis();
		System.out.println("Simple SET:" + ((end - start)/1000.0) + "seconds");
		
		//事务方式
		long start2 = System.currentTimeMillis();
		Transaction tx = jedis.multi();
		for(int i = 0; i<100000; i++){
			tx.set("t"+i, "t"+i);
			jedis.watch("t"+i);
		}
		List<Object> results = tx.exec();
		long end2 = System.currentTimeMillis();
		System.out.println("Transaction SET:" + ((end2 - start2)/1000.0) + "seconds");
		
		//管道
		Pipeline pipeline = jedis.pipelined();
		long start3 = System.currentTimeMillis();
		for(int i = 0; i<100000; i++){
			pipeline.set("p"+i, "p"+i);
		}
		List<Object> results3 = pipeline.syncAndReturnAll();
		long end3 = System.currentTimeMillis();
		System.out.println("Pipelined SET:" + ((end3 - start3)/1000.0) + "seconds");
		
		//管道中调用事务
		long start4 = System.currentTimeMillis();
		Pipeline pipeline2 = jedis.pipelined();
		pipeline2.multi();
		for(int i = 0; i < 100000; i++){
			pipeline2.set(""+i, ""+i);
		}
		pipeline2.exec();
		List<Object> results4 = pipeline2.syncAndReturnAll();
		long end4 = System.currentTimeMillis();
		System.out.println("PipeLined transaction:" + ((end - start)/1000.0) + "seconds");
		
		//分布式直连同步调用
		List<JedisShardInfo> shards = Arrays.asList(
	            new JedisShardInfo("localhost",6379),
	            new JedisShardInfo("localhost",6380));

	    ShardedJedis sharding = new ShardedJedis(shards);
	    long start5 = System.currentTimeMillis();
	    for (int i = 0; i < 100000; i++) {
	        String result = sharding.set("sn" + i, "n" + i);
	    }
	    long end5 = System.currentTimeMillis();
	    System.out.println("Simple@Sharing SET: " + ((end5 - start5)/1000.0) + " seconds");
		
//		//分布式直连异步调用
//		List<JedisShardInfo> shards2 = Arrays.asList(
//				new JedisShardInfo("localhost", 6379),
//				new JedisShardInfo("localhost", 6379));
//		ShardedJedis shardingJedis = new ShardedJedis(shards2);
//		ShardedJedisPipeline pipeline4 = sharding.pipelined();
//		long start = System.currentTimeMillis();
//		for(int i=0; i<100000; i++){
//			pipeline3.set("sp"+i, "p"+i);
//		}
//		List<Object> resultsList = pipeline4.syncAndReturnAll();
//		long end = System.currentTimeMillis();
//		System.out.println("Pipelined@Sharing SET:" + ((end5 - start5)/1000.0) + "seconds");
//
//		//分布式连接池同步调用
//		List<JedisShardInfo> shards = Arrays.asList(
//	            new JedisShardInfo("localhost",6379),
//	            new JedisShardInfo("localhost",6380));
//
//	    ShardedJedisPool pool = new ShardedJedisPool(new JedisPoolConfig(), shards);
//
//	    ShardedJedis one = pool.getResource();
//
//	    long start = System.currentTimeMillis();
//	    for (int i = 0; i < 100000; i++) {
//	        String result = one.set("spn" + i, "n" + i);
//	    }
//	    long end = System.currentTimeMillis();
//	    pool.returnResource(one);
//	    System.out.println("Simple@Pool SET: " + ((end - start)/1000.0) + " seconds");
//
//	    pool.destroy();
//
//
//	    //分布式链接池异步调用
//	    List<JedisShardInfo> shards = Arrays.asList(
//	            new JedisShardInfo("localhost",6379),
//	            new JedisShardInfo("localhost",6380));
//
//	    ShardedJedisPool pool = new ShardedJedisPool(new JedisPoolConfig(), shards);
//
//	    ShardedJedis one = pool.getResource();
//
//	    long start = System.currentTimeMillis();
//	    for (int i = 0; i < 100000; i++) {
//	        String result = one.set("spn" + i, "n" + i);
//	    }
//	    long end = System.currentTimeMillis();
//	    pool.returnResource(one);
//	    System.out.println("Simple@Pool SET: " + ((end - start)/1000.0) + " seconds");
//
//	    pool.destroy();
	    
	    
		jedis.disconnect();
		System.out.println("断开链接");
		
		
		
	}
}
