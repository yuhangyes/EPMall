package test;


import org.redisson.Redisson;
import org.redisson.config.Config;

public class RedissonTest {
    public static void main(String[] args) {
        Config config = new Config();
//        config.useClusterServers().addNodeAddress("redis://127.0.0.1:6379");
        config.useSingleServer().setAddress("redis://127.0.0.1:6379");
//        config.useSingleServer().setAddress("redis://10.122.202.25:6379");

        Redisson redissonClient = (Redisson) Redisson.create(config);
        redissonClient.shutdown();
//        RLock lock = redissonClient.getLock("myLock");
//        try {
//            lock.lock();
//            System.out.println("here");
//        }finally {
//            lock.unlock();
//            redissonClient.shutdown();
//        }
    }
}
