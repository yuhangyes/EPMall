//package com.aerospace.utils;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.apache.poi.hssf.usermodel.HSSFDateUtil;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellType;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import xin.cymall.entity.InternetStatistics;
//import xin.cymall.entity.UapInternetAdvertisingPlan;
//
//public class ExcelUtils {
//	private static Logger logger = LoggerFactory.getLogger(ExcelUtils.class);
//
//	/**
//	 * @Description: 导入统计数据
//	 * @return List<InternetStatistics>
//	 * @author hxz
//	 * @date 2018年7月10日下午1:28:39
//	 */
//	public static List<InternetStatistics> getContent(String filePath){
//		List<InternetStatistics> internetStatisticsList = new ArrayList<InternetStatistics>();
//		File file = new File(filePath);
//		Workbook wb = null;
//		if (file.exists() && file.isFile()) {
//			try {
//				if(filePath.toLowerCase().endsWith("xls")){
//					wb = new HSSFWorkbook(new FileInputStream(file));
//				}else if(filePath.toLowerCase().endsWith("xlsx")){
//					wb = new XSSFWorkbook(new FileInputStream(file));
//				}
//				Sheet sheet = wb.getSheetAt(0);
//				int rowNum = sheet.getPhysicalNumberOfRows(); //获取行数
//				if (rowNum >= 1) { //第一行为标题
//					for (int i = 1; i < rowNum; i++) {
//						System.out.println("==============="+rowNum);
//						Row row = sheet.getRow(i);
//						if (row!=null) {
////							InternetStatistics internetStatistics = new InternetStatistics();
////							// 第一列：渠道名称
////							Cell cell0 = sheet.getRow(i).getCell(0);
////							if(cell0 != null && !"".equals(cell0)){
////								cell0.setCellType(CellType.STRING);
////								String strCell0 = cell0.getStringCellValue().trim();
////								internetStatistics.setChannelCode(channelNameConvertCode(strCell0));
////							}
//
//							// 第三列：投放时间
//							Cell cell0 = sheet.getRow(i).getCell(0);
//							if(cell0 != null && !"".equals(cell0)){
//								if(!cell0.getStringCellValue().isEmpty()){
////									cell0.setCellType(CellType.NUMERIC);
////									String strCell0 = new SimpleDateFormat("yyyy-MM-dd").format(HSSFDateUtil.getJavaDate(cell0.getNumericCellValue()));
////									internetStatistics.setAdTime(strCell0);
//								}
//							}
//							// 第四列：展示次数
//							Cell cell1 = sheet.getRow(i).getCell(1);
//							if(cell1 != null && !"".equals(cell1)){
//								if(!cell1.getStringCellValue().isEmpty()){
////									cell1.setCellType(CellType.NUMERIC);
////									internetStatistics.setAdImpressions((int)cell1.getNumericCellValue());
//								}
//							}
//							// 第五列：点击次数
//							Cell cell2 = sheet.getRow(i).getCell(2);
//							if(cell2 != null && !"".equals(cell2)){
//								if(!cell2.getStringCellValue().isEmpty()){
////									cell2.setCellType(CellType.NUMERIC);
////									internetStatistics.setAdClicks((int)cell2.getNumericCellValue());
////									internetStatistics.setAdClickRate((float)internetStatistics.getAdClicks()/(float)internetStatistics.getAdImpressions());
//								}
//							}
////							// 第五列：点击率
////							Cell cell4 = sheet.getRow(i).getCell(4);
////							if(cell4 != null && !"".equals(cell4)){
////								cell4.setCellType(CellType.STRING);
////								internetStatistics.setAdClickRate(Float.valueOf(cell4.getStringCellValue()));
////							}
//							// 第六列：总花费
//							Cell cell3 = sheet.getRow(i).getCell(3);
//							if(cell3 != null && !"".equals(cell3)){
//								if(!cell3.getStringCellValue().isEmpty()){
////									cell3.setCellType(CellType.NUMERIC);
////									internetStatistics.setAdCost((float)cell3.getNumericCellValue());
////									internetStatistics.setAdPerClickCost(internetStatistics.getAdCost()/internetStatistics.getAdClicks());
////									internetStatistics.setAdCpm(internetStatistics.getAdCost()*1000/internetStatistics.getAdImpressions());
//								}
//							}
//							// 第七列：总花费（客户版）
//							Cell cell4 = sheet.getRow(i).getCell(4);
//							if(cell4 != null && !"".equals(cell4)){
//								if(!cell4.getStringCellValue().isEmpty()){
////									cell4.setCellType(CellType.NUMERIC);
////									internetStatistics.setAdCostClient((float)cell4.getNumericCellValue());
////									internetStatistics.setAdPerClickCostClient(internetStatistics.getAdCostClient()/internetStatistics.getAdClicks());
////									internetStatistics.setAdCpmClient(internetStatistics.getAdCostClient()*1000/internetStatistics.getAdImpressions());
//								}
//							}
////							// 第七列：点击单价
////							Cell cell6 = sheet.getRow(i).getCell(6);
////							if(cell6 != null && !"".equals(cell6)){
////								cell6.setCellType(CellType.STRING);
////								internetStatistics.setAdPerClickCost(Float.valueOf(cell6.getStringCellValue().trim()));
////							}
////							// 第七列：千次展示
////							Cell cell7 = sheet.getRow(i).getCell(7);
////							if(cell7 != null && !"".equals(cell7)){
////								cell7.setCellType(CellType.STRING);
////								internetStatistics.setAdCpm(Float.valueOf(cell7.getStringCellValue().trim()));
////							}
////
////							internetStatisticsList.add(internetStatistics);
//
//						}else {
//							rowNum++;
//						}
//					}
//				}
//			} catch (Exception e) {
//				logger.info("************ 解析EXCEL失败 ************");
//				e.printStackTrace();
//				return null;
//			}
//		}
//		return internetStatisticsList;
//	}
//
//	/**
//	 * @Description: 导入投放方案
//	 * @return List<InternetStatistics>
//	 * @author hxz
//	 * @date 2019年1月18日下午1:28:39
//	 */
//	public static List<UapInternetAdvertisingPlan> getAdPlanContent(String filePath){
//		List<UapInternetAdvertisingPlan> uapInternetAdvertisingPlanList = new ArrayList<UapInternetAdvertisingPlan>();
//		File file = new File(filePath);
//		Workbook wb = null;
//		if (file.exists() && file.isFile()) {
//			try {
//				if(filePath.toLowerCase().endsWith("xls")){
//					wb = new HSSFWorkbook(new FileInputStream(file));
//				}else if(filePath.toLowerCase().endsWith("xlsx")){
//					wb = new XSSFWorkbook(new FileInputStream(file));
//				}
//				Sheet sheet = wb.getSheetAt(0);
//				int rowNum = sheet.getPhysicalNumberOfRows(); //获取行数
//				if (rowNum >= 1) { //第一行为标题
//					for (int i = 1; i < rowNum; i++) {
//						Row row = sheet.getRow(i);
//						if (row!=null) {
//							UapInternetAdvertisingPlan uapInternetAdvertisingPlan = new UapInternetAdvertisingPlan();
//							// 第一列：媒体渠道
//							Cell cell0 = sheet.getRow(i).getCell(0);
//							if(cell0 != null && !"".equals(cell0)){
//								cell0.setCellType(CellType.STRING);
//								String strCell0 = cell0.getStringCellValue().trim();
//								uapInternetAdvertisingPlan.setChannelName(strCell0);
//							}
//							// 第二列：渠道产品
//							Cell cell1 = sheet.getRow(i).getCell(1);
//							if(cell1 != null && !"".equals(cell1)){
//								cell1.setCellType(CellType.STRING);
//								String strCell1 = cell1.getStringCellValue().trim();
//								uapInternetAdvertisingPlan.setChannelProduct(strCell1);
//							}
//							// 第三列：预算
//							Cell cell2 = sheet.getRow(i).getCell(2);
//							if(cell2 != null && !"".equals(cell2)){
//								cell2.setCellType(CellType.NUMERIC);
//								uapInternetAdvertisingPlan.setAmount((float)cell2.getNumericCellValue());
//							}
//							// 第四列：投放开始日期
//							Cell cell3 = sheet.getRow(i).getCell(3);
//							if(cell3 != null && !"".equals(cell3)){
//								cell3.setCellType(CellType.NUMERIC);
//								String strCell3 = new SimpleDateFormat("yyyy-MM-dd").format(HSSFDateUtil.getJavaDate(cell3.getNumericCellValue()));
//								uapInternetAdvertisingPlan.setStartTime(strCell3);
//							}
//							// 第五列：投放结束日期
//							Cell cell4 = sheet.getRow(i).getCell(4);
//							if(cell4 != null && !"".equals(cell4)){
//								cell4.setCellType(CellType.NUMERIC);
//								String strCell4 = new SimpleDateFormat("yyyy-MM-dd").format(HSSFDateUtil.getJavaDate(cell4.getNumericCellValue()));
//								uapInternetAdvertisingPlan.setEndTime(strCell4);
//							}
//
//							// 第六列：活动主题
//							Cell cell5 = sheet.getRow(i).getCell(5);
//							if(cell5 != null && !"".equals(cell5)){
//								cell5.setCellType(CellType.STRING);
//								String strCell5 = cell5.getStringCellValue().trim();
//								uapInternetAdvertisingPlan.setActivityTheme(strCell5);
//							}
//
//							uapInternetAdvertisingPlanList.add(uapInternetAdvertisingPlan);
//
//						}else {
//							rowNum++;
//						}
//					}
//				}
//			} catch (Exception e) {
//				logger.info("************ 解析EXCEL失败 ************");
//				e.printStackTrace();
//				return null;
//			}
//		}
//		return uapInternetAdvertisingPlanList;
//	}
//
//	public static String channelNameConvertCode(String channelName){
//		String channelCode = "";
//		if(channelName.equals("百度")){
//			channelCode = "IN_BAIDU";
//		}else if (channelName.equals("360")) {
//			channelCode = "IN_360";
//		}else if (channelName.equals("今日头条")) {
//			channelCode = "IN_TOUTIAO";
//		}else if (channelName.equals("抖音")) {
//			channelCode = "IN_DOUYIN";
//		}else if (channelName.equals("朋友圈")) {
//			channelCode = "IN_PENGYOUQUAN";
//		}else if (channelName.equalsIgnoreCase("Google")) {
//			channelCode = "IN_GOOGLE";
//		}
//
//		return channelCode;
//	}
//
//	public static void main(String[] args) {
//		String filePath = "http://10.122.202.222/images/2018/07/23/876540b7ec19471ea4f230654cd6c48c.xlsx";
//		File file = new File(filePath);
//		System.out.println(file.exists());
//	}
//}
