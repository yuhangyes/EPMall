package com.aerospace.utils;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedissonUtils {
    private static Logger logger = LoggerFactory.getLogger(RedissonUtils.class);

    private static RedissonUtils redissonUtils;
    private RedissonUtils(){}

    /**
     * 单例模式
     */
    public static RedissonUtils getInstance(){
        if (redissonUtils == null){
            synchronized (RedissonUtils.class){
                if (redissonUtils == null)
                    redissonUtils = new RedissonUtils();
            }
        }
        return redissonUtils;
    }

    public static RedissonClient getRedisson(Config config){
        RedissonClient redisson = Redisson.create(config);
        logger.info("成功链接RedisServer");
        return redisson;
    }

    public static RedissonClient getRedisson(String ip, String port){
        Config config = new Config();
        config.useSingleServer().setAddress(ip+":"+port);
        RedissonClient redisson = Redisson.create(config);
        logger.info("成功连接Redis Server" + "\t" + "连接" + ip + ":" + port + "服务器");
        return redisson;
    }

    public void closeRedisson(RedissonClient redisson){
        redisson.shutdown();
        logger.info("成功关闭Redis Client连接");
    }


}
