package com.aerospace.utils;

public class SearchUtils {

    //顺序查找
    public static int search(int[] array,int value){
        for (int i =0; i< array.length;i++){
            if (array[i] == value){
                return i;
            }
        }
        return -1;
    }


    //二分查找
    public static int binarySearch(int[] array,int value){
        int low = 0;
        int high = array.length -1;
        while(low<=high){
            int middle = low + ((high-low)>>1);
            if (value == array[middle]){
                return middle;
            }
            if (value>array[middle]){
                low = middle+1;
            }
            if (value < array[middle]){
                high = middle-1;
            }
        }
        return  -1;
    }


    public static void main(String[] args) {
        int[] test = new int[]{1,2,3,4,5};
        int result = binarySearch(test,3);
        System.out.println(result);
    }
}
