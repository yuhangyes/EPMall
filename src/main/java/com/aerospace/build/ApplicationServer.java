package com.aerospace.build;

import com.aerospace.controller.HomeController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
//@ComponentScan(basePackageClasses = HomeController.class)
@ComponentScan(value = "com.aerospace.*")
public class ApplicationServer {
    public static void main(String[] args){
        SpringApplication.run(ApplicationServer.class,args);
    }

}
