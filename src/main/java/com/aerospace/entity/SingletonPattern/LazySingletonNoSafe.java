package com.aerospace.entity.SingletonPattern;

public class LazySingletonNoSafe {
    //创建SingleObject的一个对象
    private static LazySingletonNoSafe instance;
    //让构造函数为private，这样该类就不会被实例化
    private LazySingletonNoSafe(){}

    //获取唯一可用的对象
    public static LazySingletonNoSafe getInstance(){
        if(instance == null){
            instance = new LazySingletonNoSafe();
        }
        return instance;
    }

    public void showMessage(){
        System.out.println("Hello World!");
    }

}
