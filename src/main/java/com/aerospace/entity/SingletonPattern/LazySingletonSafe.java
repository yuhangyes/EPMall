package com.aerospace.entity.SingletonPattern;

public class LazySingletonSafe {
    private static LazySingletonSafe instance;
    private LazySingletonSafe(){}

    public static synchronized LazySingletonSafe getInstance(){
        if (instance == null){
            instance = new LazySingletonSafe();
        }
        return instance;
    }
    public void showMessage(){
        System.out.println("Hello World!");
    }
}
