package com.aerospace.entity.SingletonPattern;

public class SingletonPatternDemo {
    public static void main(String[] args) {
        //不合法的构造函数
        //编译时错误：构造函数SingleObject()是不可见的
        //SingleObject object = new SingleObject();

        //获取唯一可用的对象
        //单例模式懒汉式非线程安全
//        LazySingletonNoSafe object = LazySingletonNoSafe.getInstance();
        LazySingletonSafe object = LazySingletonSafe.getInstance();
        object.showMessage();
    }
}
