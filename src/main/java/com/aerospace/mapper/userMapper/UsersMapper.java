package com.aerospace.mapper.userMapper;

public interface UsersMapper {
    public String getPassword(String username);

    String getRole(String username);
}
