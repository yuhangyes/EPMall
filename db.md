数据库表信息


CREATE TABLE `users` (
  `id` varchar(100) NOT NULL COMMENT '主键',
  `nickname` varchar(100) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户基础信息表

CREATE TABLE `user_auths` (
  `id` varchar(100) NOT NULL COMMENT '主键ID',
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户基础信息表主键',
  `identity_type` varchar(100) DEFAULT NULL COMMENT '登陆类型（手机号码，邮箱，用户名）或者第三方应用名称（微信，微博）',
  `identifier` varchar(100) DEFAULT NULL COMMENT '标识（手机号，邮箱，用户名或第三方应用的唯一标识）',
  `credential` varchar(100) DEFAULT NULL COMMENT '密码凭证（站内的保存密码，站外的不保存或保存token）',
  PRIMARY KEY (`id`),
  KEY `user_auths_users_fk` (`user_id`),
  CONSTRAINT `user_auths_users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户授权信息表