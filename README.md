# EPMall

<!-- #### 前言 -->
<!-- 之所以开发这套系统的原因是，在这样那样的情况下，遇到过很多熟悉某些特定技术的个体的歧视，这也变成了驱动我学习新技术的动力。术业有专攻，我觉得很多时候我们缺的不是经验和能力，而是机会。没有机会就自己创造。遂决定开发本系统，我们来研究一下现在江湖上流传的各种新技术。我们的宗旨就是，在程序员面前一切以代码码出来的东西都是纸老虎。
首先我们来罗列下目标，
1、某度娘涉及到的ooa,ood,oop思想，uml设计，Nosql,MVVM框架，React,Vue,npm,git,Nodejs,GUN linux知识，Redis，Redis集群部署,Memcached,MongoDB,restlet,Spring,SpringMVC,SpringBoot,SpringCloud,Hibernate/Mybatis,SpringSecurity,SpringData JPA,分布缓存框架，kafka,RPC框架如ICE、Dubbo，Zookeeper,SOA,Docker/Kubernetes,jquery,bootstrap,HTTP,TCP,Shell,Struts,Solr,Spark,Fluma,io,多线程，集合，分布式，缓存，消息，搜索，AOP,jvm调优，序列化，nio。（很神奇啊有没有，我居然打了个句号在这里，也许我搜罗的不全面，但看起来也是有限的啊）

2、某宝设计到的jvm，类加载机制，多线程并发，io，网络，底层中间件，分布式，云端架构技术（缓存，消息系统，热部署，容器）,设计模式，Spring/struts/ibatis,Mysql/postgresql,hadoop/hbase/storm/spark,分布式事务处理，区块链，reactive programming,http,servlet,ioc,aop,mongo,hbase,jmx,线程池,juc,rcp,activemq,tomcat,svn,cvs。

3、某鹅厂涉及到的多线程，异步，并发，mysql应用开发，Hadoop,Spark,HBase,Beam,tensorflow,es,gwt,rpc,mq,缓存技术，调用策略,hive,phoenix,kafka,zk,Storm,elk,etl。 -->

####开发环境
IDE：idea2019.1.3
数据库版本：mysql8.0.15
数据库管理工具：DBeaver6.0.0
JDK:11.0.2
redis版本：4.0.2.3
   https://my.oschina.net/yuhangyes/blog/3063186
Memcached:1.4.5
https://www.runoob.com/memcached/window-install-memcached.html


#### 介绍
基于springboot开发一套商城系统,

#### 软件架构
软件架构说明
采取springBoot架构，

####目录结构

（1）代码层的结构

　　根目录：com.aerospace

　　　　1.工程启动类(ApplicationServer.java)置于build包下

　　　　2.实体类(entity)置于entity

　　　　3.数据访问层(Dao)置于dao

　　　　4.数据服务层(Service)置于service,数据服务的实现接口(serviceImpl)至于service.impl

　　　　5.前端控制器(Controller)置于controller

　　　　6.工具类(utils)置于utils

　　　　7.常量类(constant)置于constant

　　　　8.配置信息类(config)置于config

　　　　9.数据传输类(vo)置于vo

（2）配置文件的结构

　　根目录:src/main/resources

　　　　1.配置文件(.properties/.json等)置于config文件夹下
（3）资源文件的结构
    根目录:src/main/webapp

　　　　1.通用js/css/image等置于static文件夹下

　　　　2.页面与独占js置于pages文件夹下的各自文件夹下



#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)